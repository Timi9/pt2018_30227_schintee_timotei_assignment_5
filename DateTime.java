public class DateTime {
    private Integer seconds;
    private Integer minutes;
    private Integer hours;
    private Integer days;

    public DateTime(Integer totalSeconds){
        this.seconds = totalSeconds % 60;
        this.minutes = (totalSeconds / 60) % 60;
        this.hours = totalSeconds / (60 * 60 ) % 24;
        this.days = totalSeconds / ( 60 * 60 *24);
    }

    public Integer getDays() {
        return days;
    }

    public Integer getHours() {
        return hours;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public Integer getSeconds() {
        return seconds;
    }

    public Integer transformToSeconds(){
        return (((days * 24 + hours) * 60) + minutes) * 60 + seconds;
    }

    @Override
    public String toString() {
        return days + " days " + hours + ":" + minutes + ":" + seconds;
    }
}
