import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Processor {
    private final String DESTINATION = "activities_results.txt";
    private final String SOURCE = "Activities.txt";
    private ArrayList<MonitoredData> monitoredData = new ArrayList<MonitoredData>();

    public Processor(){
        try  {
            Stream<String> stream = Files.lines(Paths.get(SOURCE));
            stream.forEach(line -> {
                String slices[] = line.split("\t\t");
                    monitoredData.add(new MonitoredData(slices[2],
                            LocalDateTime.parse(slices[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                            LocalDateTime.parse(slices[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int countDistinctDays(){
                return monitoredData
                .stream()
                .collect(Collectors.groupingBy( a -> a.getStartTime().getDayOfYear()))
                .keySet()
                .stream()
                .collect( Collectors.summingInt(a -> 1));
    }

    public Map<String, Integer> numberOfOccurrencesForEachActivityInFile(){
        return monitoredData
                .stream()
                .collect(Collectors.toMap( activity -> activity.getName(), activity -> 1, Integer::sum));
    }

    public Map<Integer, Map<String, Integer>> numberOfOccurrencesForEachACtivityInEachDayOfTheMonitoredTime(){
        return monitoredData
                .stream()
                .collect(Collectors.groupingBy(activity -> activity.getStartTime().getDayOfYear(),
                         Collectors.toMap( activity2 -> activity2.getName(), activity2 -> 1, Integer::sum)));
    }

    public Map<String, DateTime> countTheTotalTimeForEachACtivityAndFilter(){
        return monitoredData
                .stream()
                .collect(Collectors.toMap(activity -> activity.getName(), activity -> activity.getTotalDurationInSeconds(), Integer::sum))
                .entrySet()
                .stream()
                .filter( activity -> activity.getValue() > 36000)
                .collect(Collectors.toMap(activity -> activity.getKey(), activity -> new DateTime(activity.getValue())));
    }



    public static void main(String args[]){
/*


*/

        System.out.println(new DateTime((10*24)*3600+20*60*60 +59*60 +54));
        Processor p = new Processor();
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(p.DESTINATION));
            bufferedWriter.write("Task 1 ---> \n" + String.valueOf(p.countDistinctDays()) + "\n");
            bufferedWriter.write("Task 2 ---> \n" + p.numberOfOccurrencesForEachActivityInFile() + "\n");
            bufferedWriter.write("Task 3 ---> \n" + p.numberOfOccurrencesForEachACtivityInEachDayOfTheMonitoredTime() + "\n");
            bufferedWriter.write("Task 4 ---> \n" + p.countTheTotalTimeForEachACtivityAndFilter() + "\n");
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
