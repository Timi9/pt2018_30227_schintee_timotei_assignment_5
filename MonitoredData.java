import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MonitoredData {
    private String name;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    public MonitoredData( String name, LocalDateTime startTime, LocalDateTime endTime){
        this.startTime = startTime;
        this.endTime = endTime;
        this.name = name;
    }

    public int getTotalDurationInSeconds(){
        return (

                    (
                            (
                                    endTime.getDayOfYear() - startTime.getDayOfYear() //gets the day of the year to make difference
                            ) * 24 + (endTime.getHour() - startTime.getHour()) // multiplies the previous result
                            // by 24 to get the number of hours and adds the number of hours
                    ) * 60 + endTime.getMinute() - startTime.getMinute() // multiplies the previous results
                // by 60 to get the number of minutes and adds the number of minutes from difference
        ) * 60 + endTime.getSecond() - startTime.getSecond(); // multiplies the number of minutes from previous result
        // by 60 to get the number of seconds from this and adds the number of seconds from difference
    }
    public String getName(){
        return name;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    @Override
    public String toString() {
        return name + "\t\t" + startTime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss")) + "\t\t" + endTime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss"));
    }
}
